#include <iostream>

struct Password
{
	char value[16];
	bool incorrect;
	Password() : value(""), incorrect(true)
	{
	}
};

int main()
{
	std::cout << "Enter your password to continue:" << std::endl;
	Password pwd;
	std::cin >> pwd.value;

	if (!strcmp(pwd.value, "********"))
		pwd.incorrect = false;

	if(!pwd.incorrect)
		std::cout << "Congratulations\n";

	return 0;
}
// When you run the code, put BP on the first 'if', when you reach the the last if(before you enter it)
// go to the watch screen, open the 'pwd' value and replace the 'incorrect' from true to false, keep running the code
// and no matter what you enter for password it will print 'Congratulations'.